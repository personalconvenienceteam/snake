#include <stdlib.h>
#include <math.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>

#include "graphics.h"
#include "byte.h"
#include "cpu_speed.h"
#include "sprite.h"
#include "lcd.h"

#define MAX_SPEED 10
#define MAX_SNAKE_LENGTH 50
#define UP 1
#define RIGHT 2
#define LEFT 3
#define DOWN 4

void ShowWelcomeScreen();
void CreateSnake(unsigned char snakeLength);
void SpawnFood(unsigned char snakeLength);
void DisplayGameScreen(unsigned char lives, unsigned char score);
void SetupADC();
void DrawWalls();
void CheckFoodClear(unsigned char snakeLength);
unsigned char CheckWallCollision();
unsigned char ToggleWalls(unsigned char wallState);
unsigned int StartMovement();
unsigned char GetInput(unsigned char currentDirection);
unsigned char DetectFoodCollision();
unsigned int DetectSnakeCollision(unsigned char snakeLength);
void DoGameOver();
void UpdateSnake(unsigned char currentDirection, unsigned char snakeLength);
float sprite_x( Sprite* sprite );
float sprite_y( Sprite* sprite );

Sprite snakeBody[MAX_SNAKE_LENGTH];
Sprite food;

static byte snakeSegment [] = {
		BYTE( 11100000 ),
		BYTE( 11100000 ),
		BYTE( 11100000 ),
	};
	
int main(){
	// Setup teensy;
    set_clock_speed(CPU_8MHz);

	// Data Direction Registers
	DDRB = 0b01111100;
	DDRF = 0b10011111;
	DDRD = 0b11111100;

	DDRC |= 1 << PIN7;
	PORTC |= 1 << PIN7;

	// Initialize LCD
    lcd_init(LCD_DEFAULT_CONTRAST);
    clear_screen();
	srand(10);
	
	SetupADC();
	
	// Display welcome splash screen for 2 seconds
	ShowWelcomeScreen();
	clear_screen();
	
	// Initialize game data
	unsigned char lives = 5, score = 0;
	unsigned char ateFood, bitSelf;
	int speed;
	unsigned char wallState = 0, wallCollided = 0;
	// Initialize Snake
	unsigned char snakeLength = 2;
	CreateSnake(snakeLength);
		// Start Length = 2
		// Start Location = screen centre
		// Start direction = from SW1 push
		// Speed from pot
	
	// Initialize food
	SpawnFood(snakeLength);

	// Display game screen	
	DisplayGameScreen(lives, score);
	show_screen();
	// Wait for SW1 Push
	unsigned char currentDirection = StartMovement();
	
	// Main game loop
	while(1){
		ADCSRA |= 1 << ADSC;
		while (ADCSRA & (1 << ADSC));
		int16_t adc = ADC;
		speed = adc % 50 + 1;
		speed = MAX_SPEED * speed;
		_delay_ms(speed);
		show_screen();
		clear_screen();
		UpdateSnake(currentDirection, snakeLength);		
		DisplayGameScreen(lives, score);
		draw_sprite(&food);
		currentDirection = GetInput(currentDirection);

		// Check collisions
			// Increment on food collision
			ateFood = DetectFoodCollision();
			if (ateFood == 1){
				score++;
				if (wallState == 1){
					score++;
				}
				snakeLength++;
				SpawnFood(snakeLength);
			}
			
			bitSelf = DetectSnakeCollision(snakeLength);

			if (bitSelf == 1 || currentDirection == 5 || wallCollided == 1){
				clear_screen();
				lives--;
				if(lives<=0){
					DoGameOver();
				}

				snakeLength = 2;
				CreateSnake(snakeLength);
				DisplayGameScreen(lives, score);
				SpawnFood(snakeLength);
				if (wallState == 1){
					DrawWalls();
				}
				show_screen();
				currentDirection = StartMovement();
				wallCollided = 0;
			}
			

			if (wallState == 1){
				DrawWalls();
				wallCollided = CheckWallCollision();
			}

			wallState = ToggleWalls(wallState);
	}
				// +2 score walls on
			
		// Get inputs
			// Decrement lives on SW1 reverse input
			// Change snake direction on SW1
			// Show walls on SW3
			// Hide walls on SW2
			
}

void ShowWelcomeScreen(){
     draw_string(5,15,"Roderick Lenz");
	 draw_string(5,23,"N9438157");
	 draw_string(5,31,"CAB202 HEBIHEBI");
	 show_screen();
	 _delay_ms(2000);
}

void CreateSnake(unsigned char snakeLength){
	unsigned char snakeXStart = LCD_X/2;
	unsigned char snakeYStart = LCD_Y/2;
	
	for (int i = 0; i < snakeLength; i++){
		init_sprite( &snakeBody[i], snakeXStart + i*3, snakeYStart, 3, 3, snakeSegment );
		draw_sprite( &snakeBody[i] );
	}
}

void SpawnFood(unsigned char snakeLength){
	unsigned char y_min = 9, x_max = LCD_X - 3, y_max = LCD_Y - 3;

	unsigned char food_x = rand() % x_max;
	unsigned char food_y = rand() % y_max;

	if (food_y < y_min){
		food_y = y_min;
	}
	
	init_sprite( &food, food_x, food_y, 3, 3, snakeSegment );
	CheckFoodClear(snakeLength);
	draw_sprite( &food );	
}

void CheckFoodClear(unsigned char snakeLength){
	unsigned char wall1X = 0, wall1y = 28, wall1Length = 15;
	unsigned char wall2X = LCD_X, wall2y = 13, wall2Length = 10;
	unsigned char wall3X = 35, wall3y = LCD_Y, wall3Length = 12;
	unsigned char headTopX = sprite_x(&snakeBody[0]), headTopY = sprite_y(&snakeBody[0]);
	unsigned char headBottomX = headTopX + 2, headBottomY = headTopY + 2;
	unsigned char bodyTopX, bodyTopY, bodyBottomX, bodyBottomY;
	unsigned char food_x = sprite_x(&food), food_y = sprite_y(&food);

	if ((food_y <= headBottomY) && (food_y + 2 >= headTopY) && (food_x <= headBottomX) && (food_x + 2 >= headTopX)){
		SpawnFood(snakeLength);
	}
	
	for (int i = 1; i < snakeLength; i++){
		bodyTopX = sprite_x(&snakeBody[i]);
		bodyTopY = sprite_y(&snakeBody[i]);
		bodyBottomX = bodyTopX + 2;
		bodyBottomY = bodyTopY + 2;
		if ((food_y <= bodyBottomY) && (food_y + 2 >= bodyTopY) && (food_x <= bodyBottomX) && (food_x + 2 >= bodyTopX)){
			SpawnFood(snakeLength);
			}
	}
	
	if ((food_y <= wall1y) && (food_y +2 >= wall1y) && (food_x <= wall1X + wall1Length) && (food_x + 2 >= wall1X)){
		SpawnFood(snakeLength);
	}
	if ((food_y <= wall2y) && (food_y + 2 >= wall2y) && (food_x <= wall2X) && (food_x + 2 >= wall2X-wall2Length)){
		SpawnFood(snakeLength);
	}
	if ((food_y <= wall3y-wall3Length) && (food_y + 2 >= wall3y) && (food_x <= wall3X) && (food_x + 2 >= wall3X)){
		SpawnFood(snakeLength);
	}	
}

void DisplayGameScreen(unsigned char lives, unsigned char score){
	char statusBar[80];
	sprintf(statusBar, "%d (%d)", lives, score);
	draw_string( 1, 1, statusBar);
}

unsigned int StartMovement(){
	unsigned char startDirection = 0;
	while(startDirection == 0){
		startDirection = GetInput(0);
	}
	return startDirection;
}

unsigned char GetInput(unsigned char currentDirection){
	unsigned char btns, lastDirection;
	
	lastDirection = currentDirection;
	
	btns = (PIND >> 1) & 0x03;
	if (btns > 0x00 && lastDirection != DOWN) {
		_delay_ms(50);
		return UP;
	}else if (btns > 0x00 && lastDirection == DOWN){
		_delay_ms(50);
		return 5;
	}
	
	btns = (PINB >> 1) & 0x03;
	if (btns > 0x00 && lastDirection != RIGHT) {
		_delay_ms(50);
		return LEFT;
	}else if (btns > 0x00 && lastDirection == RIGHT){
		_delay_ms(50);
		return 5;
	}
	
	btns = (PINB >> PB7) & 0x03;
	if(btns > 0x00 && lastDirection != UP){
		_delay_ms(50);
		return DOWN;
	}else if (btns > 0x00 && lastDirection == UP){
		return 5;
	}
	
	btns = PIND & 0x01;
	if (btns > 0x00 && lastDirection != LEFT) {
		_delay_ms(50);
		return RIGHT;
	}else if (btns > 0x00 && lastDirection == LEFT){
		_delay_ms(50);
		return 5;
	}
	
	return currentDirection;
}

void UpdateSnake(unsigned char currentDirection, unsigned char snakeLength){	
	if( snakeLength > MAX_SNAKE_LENGTH ){
		snakeLength = MAX_SNAKE_LENGTH;
	}
	unsigned int i = snakeLength - 1;	
	unsigned char x_max = LCD_X, y_max = LCD_Y, y_min = 9;
	float head_x = 0, head_y = 0;
 
	if (currentDirection==UP){
		head_x = sprite_x(&snakeBody[0]);
		head_y = sprite_y(&snakeBody[0]) - 3;
	}else if(currentDirection==DOWN){
		head_x = sprite_x(&snakeBody[0]);
		head_y = sprite_y(&snakeBody[0]) + 3;
	}else if(currentDirection==LEFT){
		head_x = sprite_x(&snakeBody[0]) - 3;
		head_y = sprite_y(&snakeBody[0]);
	}else if(currentDirection == RIGHT){
		head_x = sprite_x(&snakeBody[0]) + 3;
		head_y = sprite_y(&snakeBody[0]);
	}

	while (i>0){
		init_sprite( &snakeBody[i], sprite_x(&snakeBody[i-1]), sprite_y(&snakeBody[i-1]), 3, 3, snakeSegment );
		i--;
	}

	if (head_x <= -3){
		head_x = x_max - 3;
	}else if ( head_x >= x_max ){
		head_x = 0;
	}
	
	if (head_y < y_min) {
		head_y = y_max - 3;
	}else if (head_y >= y_max){
		head_y = y_min;
	}
			
	init_sprite( &snakeBody[0], head_x, head_y, 3, 3, snakeSegment );
	
	for (unsigned int i=0; i<snakeLength; i++){
		draw_sprite( &snakeBody[i] );
	}
}

unsigned char DetectFoodCollision(){
	unsigned char headTopX = sprite_x(&snakeBody[0]), headTopY = sprite_y(&snakeBody[0]);
	unsigned char headBottomX = headTopX + 2, headBottomY = headTopY + 2;
	unsigned char foodTopX = sprite_x(&food), foodTopY = sprite_y(&food);
	unsigned char foodBottomX = foodTopX + 2, foodBottomY = foodTopY + 2;
	
	if ((foodTopY <= headBottomY) && (foodBottomY >= headTopY) && (foodTopX <= headBottomX) && (foodBottomX >= headTopX)){
		return 1;
	}
	return 0;
}

unsigned int DetectSnakeCollision(unsigned char snakeLength){
	unsigned char headTopX = sprite_x(&snakeBody[0]), headTopY = sprite_y(&snakeBody[0]);
	unsigned char headBottomX = headTopX + 2, headBottomY = headTopY + 2;
	unsigned char bodyTopX, bodyTopY;
	unsigned char bodyBottomX, bodyBottomY;

	for (unsigned int i = 1; i<snakeLength; i++){
		bodyTopX = sprite_x(&snakeBody[i]);
		bodyTopY = sprite_y(&snakeBody[i]);
		bodyBottomX = bodyTopX + 2;
		bodyBottomY = bodyTopY + 2;
		
		if ((bodyTopY <= headBottomY) && (bodyBottomY >= headTopY) && (bodyTopX <= headBottomX) && (bodyBottomX >= headTopX)){
			return 1;
		}	
	}
	
	return 0;
}

float sprite_x( Sprite* sprite ) {
	return sprite->x;
}

float sprite_y( Sprite* sprite ) {
	return sprite->y;
}

void DoGameOver(){
	unsigned char x_loc = 0, y_loc = 0, y_max = LCD_Y - 8, x_max = LCD_X - 45;
	while(1){
		clear_screen();
		draw_string(x_loc,y_loc,"GAME OVER");
		x_loc = rand() % x_max;
		y_loc = rand() % y_max;
		_delay_ms(1000);
		show_screen();
	}	
}

void SetupADC(){
	ADMUX = 0b01000001;
	ADCSRA |= 1 << ADEN | 1 << ADPS2 | 1 << ADPS1 | 1 << ADPS0;
}

unsigned char ToggleWalls(unsigned char wallState){
		unsigned char btns;
		btns = (PINF >> PF5) & 0x03;
		if (btns > 0x00) {
			// We have a press, wait until it's done
			_delay_ms(50);
			if (btns > 0x01) {
				return 0;
			} else {
				return 1;
			}
		}
		return wallState;
}

void DrawWalls(){
	unsigned char wall1X = 0, wall1y = 28, wall1Length = 15;
	unsigned char wall2X = LCD_X, wall2y = 13, wall2Length = 10;
	unsigned char wall3X = 35, wall3y = LCD_Y, wall3Length = 12;
	
	draw_line(wall1X,wall1y,wall1X+wall1Length,wall1y);
	draw_line(wall2X,wall2y,wall2X-wall2Length,wall2y);
	draw_line(wall3X,wall3y,wall3X,wall3y - wall3Length);
	
}

unsigned char CheckWallCollision(){
	unsigned char wall1X = 0, wall1y = 28, wall1Length = 15;
	unsigned char wall2X = LCD_X, wall2y = 13, wall2Length = 10;
	unsigned char wall3X = 35, wall3y = LCD_Y, wall3Length = 12;
	unsigned char headTopX = sprite_x(&snakeBody[0]), headTopY = sprite_y(&snakeBody[0]);
	unsigned char headBottomX = headTopX + 2, headBottomY = headTopY + 2;

	if ((wall1y <= headBottomY) && (wall1y >= headTopY) && (wall1X <= headBottomX) && (wall1X+wall1Length >= headTopX)){
		return 1;
	}
	if ((wall2y <= headBottomY) && (wall2y >= headTopY) && (wall2X-wall2Length <= headBottomX) && (wall2X >= headTopX)){
		return 1;
	}
	if ((wall3y-wall3Length <= headBottomY) && (wall3y >= headTopY) && (wall3X <= headBottomX) && (wall3X >= headTopX)){
		return 1;
	}

	return 0;
}